package main

import (
	"context"
	"flag"
	"fmt"
	"gitee.com/eoslh/eos-light-node/core/chain"
	"gitee.com/eoslh/eos-light-node/eosio"
	"gitee.com/eoslh/eos-light-node/httpserver"
	"gitee.com/eoslh/eos-p2p/p2p"
	"gitee.com/eoslh/eos-p2p/store"
	"github.com/olivere/elastic/v7"
	"go.uber.org/zap"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

var chainID = flag.String("chain-id", "bd61ae3a031e8ef2f97ee3b0e62776d6d30d4833c8f7c1645c657b149151004b", "net chainID to connect to")
var showLog = flag.Bool("v", false, "show detail log")
var startNum = flag.Int("num", 1, "start block num to sync")
var p2pAddress = flag.String("p2p", "", "p2p address")
var esAddress = flag.String("es", "127.0.0.1:9200", "es address")
var bindAddress = flag.String("bind", "0.0.0.0:8080", "http server bind address")
var genesisPath = flag.String("genesis", "./config/genesis.json", "genesis file path")

var genesis *chain.Genesis

// waitClose wait for term signal, then stop the server
func waitClose() {
	stopSignalChan := make(chan os.Signal, 1)
	signal.Notify(stopSignalChan,
		syscall.SIGINT,
		syscall.SIGKILL,
		syscall.SIGQUIT,
		syscall.SIGTERM)
	<-stopSignalChan
}

func main() {
	flag.Parse()

	if *showLog {
		logger = newLogger(false)
		//p2p.EnableP2PLogging()
	}

	var err error

	genesis, err = eosio.NewGenesisFromFile(*genesisPath)
	if err != nil {
		logger.Error("load genesis err", zap.Error(err))
		return
	}

	// from 9001 - 9020
	const maxNumListen int = 5
	peers := make([]string, 0, maxNumListen+1)

	if *p2pAddress == "" {
		for i := 0; i < maxNumListen; i++ {
			peers = append(peers, fmt.Sprintf("127.0.0.1:%d", 9001+i))
		}
	} else {
		peers = append(peers, *p2pAddress)
	}

	logger.Sugar().Infof("start %v", *startNum)
	startBlockNum := uint32(*startNum)

	ctx, cancelFunc := context.WithCancel(context.Background())

	// TODO: now it is just for test
	chains := chain.New(ctx, logger)
	if err := chains.Init(genesis); err != nil {
		logger.Error("chains init error", zap.Error(err))
		return
	}

	peersCfg := make([]*p2p.PeerCfg, 0, len(peers))
	for _, p := range peers {
		peersCfg = append(peersCfg, &p2p.PeerCfg{
			Address: p,
		})
	}

	var esClient *elastic.Client
	esAds := *esAddress
	if len(esAds) != 0 {
		if !strings.Contains(esAds, "http://") {
			esAds = "http://" + esAds
		}
		esClient, err = elastic.NewClient(
			elastic.SetURL(esAds),
			elastic.SetSniff(false),                        //允许您指定弹性是否应该定期检查集群（默认为真）
			elastic.SetHealthcheckInterval(10*time.Second), //指定间隔之间的两个健康检查（默认是60秒）
			elastic.SetGzip(true),                          //启用或禁用请求端的压缩。默认情况下禁用
			elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)))
		//elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)))
		if err != nil {
			logger.Error("es start error", zap.Error(err))
			return
		}
	} else {
		esClient = nil
	}

	storer, err := store.NewBBoltStorer(logger, *chainID, "./blocks.db", false, esClient, startBlockNum)
	if err != nil {
		logger.Error("new storer error", zap.Error(err))
		return
	}

	client, err := p2p.NewClient(
		ctx,
		*chainID,
		peersCfg,
		p2p.WithNeedSync(startBlockNum),
		p2p.WithLogger(logger),
		p2p.WithStorer(storer),
		p2p.WithHandler(&chainP2PHandler{
			chain: chains,
		}),
	)

	if err != nil {
		logger.Error("p2p start error", zap.Error(err))
		return
	}

	go httpserver.RunHttpServer(*bindAddress, esClient)

	// wait close node
	waitClose()

	// cancel context
	logger.Info("start close node")
	cancelFunc()

	logger.Info("wait p2p peers closed")
	client.Wait()

	logger.Info("wait chain closed")
	chains.Wait()

	logger.Info("wait storer closed")
	storer.Close()
	storer.Wait()

	logger.Info("light node closed success")
}

//docker run -d --name elasticsearch   -p 9200:9200 -p 9300:9300  -e "discovery.type=single-node"  -e "network.bind_host=0.0.0.0"  -e "network.publish_host=0.0.0.0"  -e "transport.host=0.0.0.0"   elasticsearch:7.14.2
