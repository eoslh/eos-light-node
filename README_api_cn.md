# **EOS轻节点 api**



## 概述


### 1.文档概述
本文档描述了EOS轻节点的所有接口文档以及接口功能
### 2.缩略语与术语
| 序号 | 缩略语 | 全称 | 中文译名 | 补充说明 |
| --- | --- | --- | --- | --- |
| 1 |


## 一、接口

## 1.查询块接口

#### 请求方式：GET

#### 请求地址： 
>/v1/chain/get_block

#### 请求参数：
| **name** | **type** | **isRequired** | **desc** |
| --- | --- | --- | --- |
| block\_num | int | N | 区块号（区块高度） |
| block\_id | int | N | 区块ID |
<font color="red">
注：两个参数同时存在时，接口优先判断参数block_num，block_id参数无效
</font>

#### 返回参数Response：
| **name** | **type** | **desc** |
| --- | --- | --- |
| block_num | string | 区块号（区块高度） |
| block_id | string | 区块ID |

例：
#### 请求入参：
>http://localhost:18088/v1/chain/get_block?block_num=1

#### 返回信息：
```json
{
  "block_num" : "1",
  "block_id" : "0000000000000000000000000000000000000000000000000000000000000000"
} 
```