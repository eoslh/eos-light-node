module gitee.com/eoslh/eos-light-node

go 1.12

require (
	gitee.com/eoslh/eos-p2p v0.1.5
	github.com/eapache/channels v1.1.0 // indirect
	github.com/eapache/queue v1.1.0 // indirect
	github.com/eoscanada/eos-go v0.8.16
	github.com/eosspark/eos-go v0.0.0-20190820132053-c626c17c72f9
	github.com/gin-gonic/gin v1.7.4
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/olivere/elastic/v7 v7.0.29
	github.com/pkg/errors v0.9.1
	github.com/tidwall/gjson v1.11.0 // indirect
	github.com/tidwall/sjson v1.2.3 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.10.0
)
